#-*- coding:utf-8 -*-

import numpy
import time
import pygame
from math import *
import generateur as Generateur


def play(freq_p =440, duree_p= 0.5): # jouer une note
    pygame.mixer.pre_init(44100, -16, 1) # initialisation du mixer pg
    duree = duree_p
    duree *=44100 # transformation de la duree en nombres d'echantillons
    duree = int(duree) # on le converti en entier (par securite)
    pygame.init() # on initialise le module pg
    freq_echantill = 44100.0 # frequence d'echantillonage
    ymax = 20000 # ymax
    freq = freq_p # frequence

    longu = freq_echantill / freq # calcul de la longueur d'onde
    x = numpy.arange(int(longu)) * (2 * pi /longu) # formule de omega

    sin_x = list(x) # on etablit une liste aussi grande que xvalues
    for i, contenant in enumerate(x): # on dresse avec sinus
        sin_x[i] = ymax * sin(contenant)

    y = numpy.array(sin_x, dtype=numpy.int16) # on les cast en int16 (necessaire à PG)
    y = numpy.resize(y, duree) # on le repete en fonction de notre duree
    son = pygame.sndarray.make_sound(y) # on le transforme en son
    son.play() # on le joue
    time.sleep(0.4) # on ajoute un delai

def donne_frequence(note_p = 'La'):
    la_d = Generateur.getRankDec('La') # rang de la
    note_d = Generateur.getRankDec(note_p) # rang de la note
    if la_d == note_d: # si la note est la
        return 440.00
    elif la_d < note_d: # si elle est au dessus, on multiplie freq_la par la formule
        ecart = note_d - la_d
        return 440.00 * (2**(2*ecart/12))
    elif la_d > note_d: # si elle est en dessous, on la divise
        ecart = la_d - note_d
        return 440.00 / (2**(2*ecart/12))

def donne_frequences_list(notes_p, mode_p):
    frequences = list(notes_p) # on liste les notes dans la liste frequence
    mode = list(Generateur.donne_mode(mode_p)) # on liste les modes
    tonalite = donne_frequence(notes_p[0]) # on cherche la tonalite
    frequences[0] = tonalite # frequence de la fondamentale
    for i, content in enumerate(notes_p):
        if i != 0: # on exclu la fond (deja faite)
            rang = mode[i] # on cherche le rang
            frequences[i] = tonalite * 2 **(2*rang/12) # on calcule par rapport à la fondamentale
    return frequences

def jouer_gamme(frequences_p):
    for i, content in enumerate(frequences_p):
        play(content)  # on joue la liste et on y accolle un delai
        time.sleep(0.1)

if __name__ == '__main__': # tests
    play()

