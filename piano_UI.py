# -*- coding: utf-8 -*-
import Tkinter as Tk
import generateur as Generateur


""" ------------------------------Affichage pour le piano------------------------
"""

class piano_UI(Tk.Frame):
    def __init__(self, parent):
        Tk.Frame.__init__(self, parent) #constructeur parent
        self.parent = parent # parent eventuel
        self.canvas = Tk.Canvas(self.parent, width=550, height=150,
                             background= "white")  # canvas
        self.init_canvas() #initialisation du canvas

    def init_canvas(self):
        self.piano = Tk.PhotoImage(file="img/piano.gif") # image gif clavier
        self.canvas.create_image(275, 65, image=self.piano) # on colle l'image
        self.canvas.pack() # on pack le canvas

    def generer_even(self, tonalite, mode):
        self.canvas.delete(Tk.ALL) # on supprime tout
        self.init_canvas() #on reinitialise le canvas
        self.liste_notes = Generateur.generer_gamme(tonalite, mode) # on genere la gamme
        for i, contenant in enumerate(self.liste_notes): # enumeration de la liste des notes
            note_dec = Generateur.getRankDec(contenant) # note decimale

            if contenant == tonalite: # coloration de la fondamentale
                couleur = '#ff0000'
            else:
                couleur = '#ffffff'

            if contenant.find('#') != -1:   # becarre
                y = 60                      # ordonnée
                x = note_dec * 19 + 138 # abscisse
                self.canvas.create_oval(x, y, x+8, y+8, fill=couleur) # on cree un oval
                self.canvas.create_oval(x+114, y, x+122, y+8, fill=couleur) # et son octave
            else:  # idem mais pour les dieses
                y = 90
                x = note_dec * 19 + 133
                self.canvas.create_oval(x, y, x+8, y+8, fill=couleur)
                self.canvas.create_oval(x+121, y, x+129, y+8, fill=couleur)


# tests
if __name__ == '__main__':
    root = Tk()
    piano_UI(root)
    root.mainloop()
