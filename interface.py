# -*- coding: utf-8 -*-

import tkMessageBox as Messagebox # fenetres de dialogue
import Tkinter as tk # graphique lib
import donnees as Donnees # donnees globales du programme
import accordeur_UI  as Accordeur # systeme de l'accordeur
import generateur_UI as Generateur_gammes # commandes du generateur
import editeur_UI as Editeur # systeme de l'editeur
import tkMessageBox as MessageBox

"""
Classe implémentant le GUI (interface graphique)
de notre programme.
"""

class Interface(tk.Tk):

    def __init__(self, parent): # constructeur
        tk.Tk.__init__(self, parent) # liaison avec constructeur parent
        self.parent = parent # possibilité de rajouter un parent
        icone = tk.PhotoImage(file='img/icone.gif')
        self.tk.call('wm', 'iconphoto', self._w, icone)
        self.cadre_secondaire = None # par defaut cadre secondaire est vide
        self.init_menu() # appel de la methode pour initialiser cadre principale

    def init_menu(self):
        # si on vient du cadre secondaire,
        # on "oublie" celui-ci en remettant tout à l'etat init
        if self.cadre_secondaire is not None:
            self.cadre_secondaire.pack_forget()
            self.cadre_secondaire = None
            self.bouton_quitter.pack_forget()

        self.modes = Donnees.donner_modes(Donnees.lire_fichier()) # initialise la liste des modes
        self.geometry("250x200+500+250") # dimensions fenetre
        self.title("iMusique")
        """ MENU PRINCIPAL """
        self.cadre_primaire = tk.Frame(self.parent)
        tk.Label(self.cadre_primaire, text="Bienvenue dans iMusique").pack(pady=2) # titre
        # Ensemble de boutons qui forment le contenu du cadre prinipal principale
        tk.Button(self.cadre_primaire, text="Generateur de gammes",
                                    command=self.init_gammes).pack(pady=2)
        tk.Button(self.cadre_primaire, text="Editeur de gammes",
                                    command=self.init_edit).pack(pady=2)
        tk.Button(self.cadre_primaire, text="Accordeur",
                                    command=self.init_accordeur).pack(pady=2)
        tk.Button(self.cadre_primaire, text="aide",
                                    command=self.helpMessageBox).pack(pady=2)
        self.bouton_quitter = tk.Button(self.parent, text="Quitter",
                                    command=self.quitter)

        self.cadre_primaire.pack() # pack du cadre principale
        self.bouton_quitter.pack(pady=2)

    def helpMessageBox(self):
        # boite d'aide
        Messagebox.showinfo("aide", """Ce programme permet de générer des gammes sur une partition,
        un manche de guitare ou un clavier de piano.
        Si vous souhaitez rajouter des gammes, veuillez cliquer sur editeur de gammes dans le menu,
        et entrer votre gamme en Do et sous format Note1-Note2-Note3
        Un accordeur est aussi à votre disposition...""")

    def init_gammes(self):
        self.geometry("550x300+350+250") # dimensions
        self.title("generateur de gamme")
        self.cadre_primaire.pack_forget() # on "oublie" cadre principal
        self.bouton_quitter.pack_forget() # on "oublie" le bouton quitter

        """ contenu frame secondaire: portée musicale et cle de sol """
        self.cadre_secondaire = tk.Frame(self.parent)
        # on lance le controleur du generateur
        Generateur_gammes.generateur_UI(self.cadre_secondaire).pack()

        tk.Button(self.cadre_secondaire, text="retour",  # retour
                           command= self.init_menu).pack()

        self.cadre_secondaire.pack() #packs
        self.bouton_quitter.pack()


    def init_edit(self):
        self.geometry("550x320+350+250") # dimensions
        self.title("generateur de gamme")
        self.cadre_primaire.pack_forget() # on "oublie" cadre principal
        self.bouton_quitter.pack_forget() # on "oublie" le bouton quitter

        self.cadre_secondaire = tk.Frame(self.parent)
        self.editeur = Editeur.editeur_UI(self.cadre_secondaire) #editeur
        self.editeur.pack(fill=tk.BOTH)
        tk.Button(self.cadre_secondaire, text="retour",  # retour
                           command= self.init_menu).pack()

        self.cadre_secondaire.pack() #packs
        self.bouton_quitter.pack()

    def init_accordeur(self):
        self.geometry("250x200+500+250") # dimensions
        self.title("accordeur")
        self.cadre_primaire.pack_forget() # on "oublie" cadre principal
        self.bouton_quitter.pack_forget()  # on "oublie" le bouton quitter

        self.cadre_secondaire = tk.Frame(self.parent)
        Accordeur.accordeur_UI(self.cadre_secondaire).pack()  #accordeur
        tk.Button(self.cadre_secondaire, text="retour",
                           command= self.init_menu).pack()
        self.cadre_secondaire.pack() #packs
        self.bouton_quitter.pack()

    def quitter(self):
        MessageBox.showinfo("Merci", """ Merci d'avoir utilisé notre application !""")
        self.destroy()
