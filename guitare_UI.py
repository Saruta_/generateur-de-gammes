# -*- coding: utf-8 -*-


import Tkinter as Tk
import generateur as Generateur
import donnees as Donnees

""" -----------------affichage manche guitare-----------------------------------
"""

class guitare_UI(Tk.Frame):
    def __init__(self, parent):
        Tk.Frame.__init__(self, parent)
        self.parent = parent #constructeur parent
        self.canvas = Tk.Canvas(self.parent, width=550, height=150,
                             background= "white")  # canvas
        self.init_canvas() # initialisation du canvas

    def init_canvas(self):
        self.manche_guitare = Tk.PhotoImage(file="img/manche_guitare.gif") # gif manche guitare
        self.canvas.create_image(275, 65, image=self.manche_guitare) # collage de l'image
        self.canvas.pack() # pack de l'image

    def generer_even(self, tonalite, mode):
        self.canvas.delete(Tk.ALL) # on supprime tout le contenu du canvas
        self.init_canvas() # on le reinitialise
        self.liste_notes = Generateur.generer_gamme(tonalite, mode) # on genere la gamme
        for i, contenant in enumerate(self.liste_notes): # on l'enumere

            if contenant == tonalite: # coloration fondamentale
                couleur = '#ff0000'
            else:
                couleur = '#ffffff'

            y = 91 # ordonnée
            x1, x2, x3 = Donnees.CORDE1[contenant], Donnees.CORDE2[contenant], Donnees.CORDE3[contenant]
            x4, x5, x6 = Donnees.CORDE4[contenant], Donnees.CORDE5[contenant], Donnees.CORDE6[contenant]
            #abscisses, le chiffre correspond au numero de la corde

            self.canvas.create_oval(x1, y, x1+8, y+8, fill=couleur) # fonctions de collage
            self.canvas.create_oval(x2, y-8-(x2/90), x2+8, y+8-8-(x2/90), fill=couleur)
            self.canvas.create_oval(x3, y-16-(x3/70), x3+8, y+8-16-(x3/70), fill=couleur)
            self.canvas.create_oval(x4, y-24-(x4/60), x4+8, y+8-24-(x4/60), fill=couleur)
            self.canvas.create_oval(x5, y-32-(x5/50), x5+8, y+8-32-(x5/50), fill=couleur)
            self.canvas.create_oval(x6, y-39-(x6/30), x6+8, y+8-39-(x6/30), fill=couleur)



if __name__ == '__main__': # tests
    root = Tk()
    guitare_UI(root)
    root.mainloop()
