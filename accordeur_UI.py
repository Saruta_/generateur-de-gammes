# -*- coding: utf-8 -*-
"""
Created on Tue Apr 29 17:25:42 2014

@author: saruta
"""

import Tkinter
import ttk
import donnees as Donnees
import son as Son
import tkMessageBox as MessageBox

"""
-----------------------Accordeur------------------------------
"""

class accordeur_UI(Tkinter.Frame):
    def __init__(self, parent):
        Tkinter.Frame.__init__(self, parent) # utilisation du constructeur parent
        self.parent = parent # eventuel parent

        self.accordeur_var = Tkinter.StringVar() # variable necessaire a combobox

        self.cadre_note = Tkinter.Frame(self.parent) # cadres pour la forme
        self.cadre_duree = Tkinter.Frame(self.parent)

        Tkinter.Label(self.cadre_note, text="Note: \t ").pack(side=Tkinter.LEFT) # texte devant note
        self.comboBox_tuner = ttk.Combobox(self.cadre_note, textvariable=self.accordeur_var,
                                values=Donnees.NOTES, state='readonly') # liste deroulante en lecture seule (notes)
        self.comboBox_tuner.set(Donnees.NOTES[0]) # choix par defaut
        self.comboBox_tuner.pack() # packs
        self.cadre_note.pack(pady=3)

        Tkinter.Label(self.cadre_duree, text='Duree (s) : ').pack(side=Tkinter.LEFT) # texte devant duree
        self.spinbox = Tkinter.Spinbox(self.cadre_duree, from_= 1, to=12) # choix de la duree
        self.spinbox.pack() # packs
        self.cadre_duree.pack(pady=3)

        Tkinter.Button(self.parent, text="Jouer", command=self.jouer_accordeur).pack() # bouton jouer

    def jouer_accordeur(self): # evenement du bouton jouer
        note = self.comboBox_tuner.get() # reprise de la note
        duree = self.spinbox.get() # reprise de la duree
        """if (duree > 12) or (duree < 1):
            self.erreur_format_longueur()
        else:"""
        frequence = Son.donne_frequence(note) # on cherche la frequence
        Son.play(frequence, float(duree)) # on joue (sans oublier le cast en flottant!)

    def erreur_format_longueur(self):
        MessageBox.showerror("Erreur", """ erreur format ou longueur """)


if __name__ == '__main__': # code de tests
    root = Tkinter.Tk()
    accordeur_UI(root)
    root.mainloop()
