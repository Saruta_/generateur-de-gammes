# -*- coding: utf-8 -*-

import donnees as Donnees
""" ---------------Algorithmes generant les gammes ----------------------------
"""

#retourne le rang de la note en decimal en fonction
#de la list de notes et de note en str    
def getRankDec(note_p): 
    i = 0
    while i <= 6:
        if note_p == getTabNotes(i):
            return i
        i = i + 0.5        
    return -1.0            
    
#module la gamme en fonction de la tonalite   
def modTone(tab_output_p, tone_p):
    for i, content in enumerate(tab_output_p):
        tab_output_p[i] += tone_p 
    return tab_output_p     
    
#enumerations de la suite de note dans un dictionnaire  
def getTabNotes(note_dec):    
    tab_notes = {0.0:'Do',0.5:'Do#',1.0:'Re',1.5:'Re#',2.0:'Mi',
                 2.5:'Fa',3.0:'Fa#',3.5:'Sol',4.0:'Sol#',4.5:'La',
                 5.0:'La#',5.5:'Si',6.0:'Do',6.5:'Do#',7.0:'Re',
                 7.5:'Re#',8.0:'Mi',8.5:'Fa',9.0:'Fa#',9.5:'Sol',
                 10.0:'Sol#',10.5:'La',11.0:'La#',11.5:'Si'}
    return tab_notes[note_dec]
def donne_mode(mode_p): # retourne un mode en fonction de son nom
    liste_fichier = Donnees.lire_fichier()
    return Donnees.donner_gamme(liste_fichier, mode_p)
    
def donne_gamme_dec(gamme_notes): # retourne gamme en decimal grace aux notes
    gamme_dec = [] 
    for i, content in enumerate(gamme_notes):    
        gamme_dec.append(getRankDec(content))
    return gamme_dec        
    
def generer_gamme(tonalite_p, mode_p):
    tab_scale_output = donne_mode(mode_p) # on attrape le mode
    tab_scale_output = modTone(tab_scale_output, getRankDec(tonalite_p))
    # on module en fonction de la tonalite voulue    
    
    liste_sortie = []
    for i, content in enumerate(tab_scale_output): #affichage list finale
        liste_sortie.append(getTabNotes(content))  
    # on transforme tout ca en notes
    return liste_sortie              
  
if __name__ == "__main__": # tests
    print donne_mode('Majeur')  
    raw_input('...')   
 
       
