# -*- coding: utf-8 -*-


import tkMessageBox as MessageBox
import Tkinter as Tk
import ttk
import donnees as Donnees
import generateur as Generateur
import gammes_UI as Gammes
import guitare_UI as Guitare
import piano_UI as Piano
import son as Son

"""-------------controleur des differents modules pour afficher les gammes----------------------------
"""
class generateur_UI(Tk.Frame):
    def __init__(self, parent): 
        Tk.Frame.__init__(self, parent) # constructeur parent
        self.parent = parent # eventuel parent visuel
        self.gamme = [] # liste gamme generee vide
        self.mode = '' # mode vide
        self.modes = Donnees.donner_modes(Donnees.lire_fichier()) # on attribue les modes du fichier
        self.init_notebook() # initialisation des onglets
        self.init_commandes() # initialisation des commandes
        
    def init_notebook(self):
        self.notebook = ttk.Notebook(self.parent) # onglets
        self.tab1 = Tk.Frame(self.notebook) # decoupage en 3 onglets
        self.tab2 = Tk.Frame(self.notebook) 
        self.tab3 = Tk.Frame(self.notebook)
        
        self.gammes = Gammes.gammes_UI(self.tab1) # creation des differents affichages
        self.guitare = Guitare.guitare_UI(self.tab2)
        self.piano = Piano.piano_UI(self.tab3)

        self.notebook.add(self.tab1, text='partition') # attributions des affichages aux onglets
        self.notebook.add(self.tab2, text='guitare')
        self.notebook.add(self.tab3, text='piano')
        
        self.notebook.pack() # packs            
        self.gammes.pack()        
        self.piano.pack()
        self.guitare.pack()         
        
    def init_commandes(self):
        self.combo_tonalite = Tk.StringVar() # variables pour les listes deroulantes
        self.combo_mode = Tk.StringVar()             
        
        cadre_combo = Tk.Frame(self.parent) # cadre des listes deroulantes                
        
        self.comboBox_tonalite = ttk.Combobox(cadre_combo, textvariable=self.combo_tonalite, values=Donnees.NOTES, 
                                 state='readonly')      # liste deroulante pour les tonalite 
        self.comboBox_mode = ttk.Combobox(cadre_combo, textvariable=self.combo_mode, values=self.modes, 
                                 state='readonly')      # liste deroulante pour les modes 
        
        self.comboBox_tonalite.set(Donnees.NOTES[0])    # notes de base des listes deroulantes
        self.comboBox_mode.set(self.modes[0])
        
        self.comboBox_tonalite.pack(side=Tk.LEFT) # packs
        self.comboBox_mode.pack(side=Tk.RIGHT)
        cadre_combo.pack()

        cadre_boutons = Tk.Frame(self.parent) # cadre boutons
        Tk.Button(cadre_boutons, text="Generer!", # bouton generer
               command=self.generer).pack(side=Tk.LEFT)
        Tk.Button(cadre_boutons , text="Play!", # bouton jouer
               command=self.jouer).pack(side=Tk.RIGHT)                           
               
        cadre_boutons.pack()      # pack              
        
    def generer(self):
        self.mode = self.comboBox_mode.get() # recuperation du mode
        self.tonalite = self.comboBox_tonalite.get() # recuperation de la tonalite
        self.gamme = Generateur.generer_gamme(self.tonalite, self.mode) # on genere la gamme
        self.gammes.generer_even(self.tonalite, self.mode) # on genere les affichages
        self.guitare.generer_even(self.tonalite, self.mode)
        self.piano.generer_even(self.tonalite, self.mode)  
        
    def jouer(self):
        if self.gamme != []: 
            print self.mode
        # si le mode n'est pas vide, on cherche les frequences et on les joue
            frequences = Son.donne_frequences_list(self.gamme, self.mode) 
            Son.jouer_gamme(frequences)
        else: # sinon erreur
            MessageBox.showerror("erreur", """Aucune gamme n'est générée...""") 
        
if __name__ == '__main__': # tests
    root = Tk() 
    generateur_UI(root)
    root.mainloop()  