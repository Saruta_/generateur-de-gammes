# -*- coding: utf-8 -*-

import Tkinter as Tk
import ttk
import donnees as Donnees
import generateur as Generateur
import tkMessageBox as MessageBox

""" -----------------------EDITEUR DE GAMMES--------------------------------
"""
class editeur_UI(Tk.Frame):
    def __init__(self, parent):
        Tk.Frame.__init__(self, parent) # constructeur parent
        self.parent = parent # attribution d'un eventuel parent

        self.partie_superieur = Tk.Frame(self.parent) # partie superieur de la fenetre
        self.donnees = Donnees.lire_fichier() # lecture des differentes gammes et noms (liste de stockage)
        
        self.entry_nom = Tk.Entry(self.partie_superieur) # entree et packs du champ "nom"
        self.entry_nom.pack(side=Tk.LEFT, padx=5) 
        
        self.entry_notes = Tk.Entry(self.partie_superieur) #entree et packs du champ notes
        self.entry_notes.pack(side=Tk.LEFT, padx=5)
        
        Tk.Button(self.partie_superieur, text='Supprimer', command=self.supprimer_gamme).pack(side=Tk.RIGHT, padx=5)
        Tk.Button(self.partie_superieur, text='Ajouter', command=self.ajouter_gamme).pack(side=Tk.RIGHT, padx=5) 
        # boutons ajouter et supprimer 
        
        self.partie_superieur.pack(pady=10) 
        self.liste() # initialisation de la liste de gammes dans le widget
        
    def liste(self):    
        self.bar = Tk.Scrollbar(self.parent) # scrollbar d'experimentation (non optimisant)
        self.bar.pack(side=Tk.RIGHT, fill='y')
        
        self.treeview = ttk.Treeview(self.parent, yscrollcommand=self.bar.set) # definition de l'arbre
        self.bar.configure(command = self.treeview.yview) # ajout du scrollbar à l'arbre
        self.treeview.pack(expand=True, fill=Tk.BOTH) 
        
        self.treeview['columns'] = ('notes') # ajout d'une colonne pour les notes
        self.treeview.column('notes', width=400) # largeur de la colonne
        self.treeview.heading('notes', text='notes pour do') # attribution d'un texte d'entete
        
        self.actualiser_treeview() # on l'actualise pour afficher les gammes dans l'arbre       

    def actualiser_treeview(self):
        map(self.treeview.delete, self.treeview.get_children()) # supprime tous les enfants de l'arbre 
        for i, contenant in enumerate(Donnees.donner_modes(self.donnees)): # enumeration des modes du fichier
            string_gamme = ""       # futur chaine qui va être affichée
            liste_gamme = Generateur.generer_gamme('Do', contenant) # on chercher la gamme de Do correspondante           

            for j, contenant_b in enumerate(liste_gamme): # on enumere cette gamme 
                if j != 0:                # ajout d'un tiret avant la note sauf pour la premiere
                    string_gamme += '-'
                string_gamme += contenant_b # on ajoute la note
            self.treeview.insert("", i, text=contenant, values=(string_gamme)) # on insere la gamme dans l'arbre
            #text -> txt colonne nom de la gamme, values -> txt colonne gammes

    def ajouter_gamme(self):
        notes_str = self.entry_notes.get() # on recupere le texte du champ des notes
        notes_liste = notes_str.split('-') # on divise en liste, chaque item etant separe par un tiret

        valide = True
        for i, contenant in enumerate(notes_liste): # on transforme la liste avec des valeurs numeriques
            if contenant == 'Do' and i != 0:  # mise à l'octave pour le dernier do
                notes_liste[i] = 6.0                
            else:
                notes_liste[i] = Generateur.getRankDec(contenant) # on extrait le rang de la note 
            if (notes_liste[i] == -1.0 or i >= 13) or (self.entry_nom.get() == ''): # test de la validite de la note 
                valide = False
        if valide:                
            self.donnees.append(self.entry_nom.get()) # on ajoute le nom à la liste principale
            self.donnees.append(notes_liste) # puis les notes
            Donnees.actualiser_fichier(self.donnees) # on actualise le fichier
            self.actualiser_treeview() # on actualise l'arbre                
        else:
            if self.entry_nom.get() == '':
                self.non_rempli()
            else:
                self.non_valide()   # message d'erreur validite             
        
    def supprimer_gamme(self):
        if self.treeview.item(self.treeview.selection())['text'] in Donnees.MODES_BASIQUES:
            self.suppression_non_autorise() 
        # au cas ou l'utilisateur veut supprimerune gamme de base
        elif self.treeview.selection() != '': # si slection non vide
            if MessageBox.askyesno("Suppression", """ Etes-vous sûre de supprimer cette gamme?"""):
                item = self.treeview.item(self.treeview.selection()) # on recupere l'item
                nouvelle_liste_gammes = Donnees.enlever_gamme(item['text'], self.donnees) 
                # on enleve de la liste principale
                Donnees.actualiser_fichier(nouvelle_liste_gammes) # on actualise le fichier
                self.actualiser_treeview() # on actualise  l'arbre
            else:
                pass
        else:
            self.non_selectionne() #erreur de selection

    def non_valide(self): # affichage d'une fenetre, pour erreur de validite                                     
        MessageBox.showerror("Erreur", """Le format entré n'est pas valide, veuillez 
entrer une liste de notes sans espaces espacés par des tirets, seuls les dièses sont autorisés
Les notes doivent appartenir à: Do-Do#-Re-Re#-Mi-Fa-Fa#-Sol-Sol#-La- La#-Si, le maximum est de 13 notes""")     

    def non_rempli(self):
        MessageBox.showerror("Erreur", """ Au moins l'un de vos champs est vide.""")
       
    def non_selectionne(self): # affichage d'une fenetre, pour erreur de selection
        MessageBox.showerror("Erreur", """ Vous n'avez pas selectionné de gamme.""")
    
    def suppression_non_autorise(self): # en cas de tentative de suppression non autorise
        MessageBox.showerror("Erreur", """ La suppresion des gammes de base est interdites.""")    
    
if __name__ == '__main__': # code de test
    root = Tk() 
    editeur_UI(root).pack
    root.mainloop()         
        
