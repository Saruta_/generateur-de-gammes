# -*- coding: utf-8 -*-


import pickle

def donner_modes(GAMMES_p):
    """ Retourne le nom des gammes en fonction 
    de la liste du fichier datas.bin .
    Le nom correspond aux elements paires de 
    ce fichier. """
    noms = []
    for i, content in enumerate(GAMMES_p):
        if i%2 == 0:
            noms.append(content)
    return noms        
        
def donner_gamme(GAMMES_p, nom_p):
    """ Retourne la gamme en fonction 
    de la liste du fichier datas.bin 
    et du nom de celle-ci.
    Cette gamme correspond à l'element  
    suivant son nom. 
    ex: ['MAJEUR', gamme_majeur, 'MINEUR', 
    gamme_mineur etc...]    
    """
    for i, content in enumerate(GAMMES_p):
        if i%2 == 0:
            if nom_p == content:
                return GAMMES_p[i+1]
    return -1    
        
def generer_donnees(donnees, nom, gamme):
    """ ajout d'une gamme (nom + gamme interne)"""
    donnees.append(nom)
    donnees.append(gamme)
    return donnees        
        
def actualiser_fichier(donnees):
    """ Actualisation par dump binaire de la 
    liste (= serialisation)."""
    with open('datas.bin', 'wb') as fichier:
        pickler = pickle.Pickler(fichier)
        pickler.dump(donnees)
        
def lire_fichier():
    """ Lecture de la liste presente 
    dans le fichier. """
    with open('datas.bin', 'rb') as fichier:
        depickler = pickle.Unpickler(fichier)
        return depickler.load() 
 
def enlever_gamme(nom, gammes):
    """ Suppression de la liste interne (gamme)
    puis du nom """
    passe_sur_nom = False
    for i, content in enumerate(gammes):
        if passe_sur_nom :
            gammes.remove(content)
            break
        if content == nom:
            passe_sur_nom = True
    gammes.remove(nom)                
    return gammes          
       

""" CONSTANTES DU PROGRAMME """

"""tuples (constantes) NOTES""" 
NOTES = ('Do', 'Do#', 'Re', 'Re#', 'Mi', 'Fa', 'Fa#', 
         'Sol', 'Sol#', 'La', 'La#', 'Si')
""" sauvegarde de la liste des modes principaux """         
MODES_BASIQUES = ('Majeur', 'Mineur naturelle', 'Mineur melodique', 
         'Mineur harmonique', 'Pentatonique', 'Blues')         
COORD = {'Do': 75, 'Re': 70, 'Mi': 65, 'Fa': 60, 'Sol': 55,
         'La': 50, 'Si': 45}         

CORDE1 = {'Mi': 35, 'Fa': 60,'Fa#': 90, 'Sol': 130, 'Sol#': 160, 'La': 190, 'La#': 220, 
          'Si': 250, 'Do': 280, 'Do#': 300, 'Re': 330, 'Re#': 350}
CORDE2 = {'La': 35, 'La#': 60,'Si': 90, 'Do': 130, 'Do#': 160, 'Re': 190, 'Re#': 220, 
          'Mi': 250, 'Fa': 280, 'Fa#': 300, 'Sol': 330, 'Sol#': 350}          
CORDE3 = {'Re': 35, 'Re#': 60,'Mi': 90, 'Fa': 130, 'Fa#': 160, 'Sol': 190, 'Sol#': 220, 
          'La': 250, 'La#': 280, 'Si': 300, 'Do': 330, 'Do#': 350}  
CORDE4 = {'Sol': 35, 'Sol#': 60,'La': 90, 'La#': 130, 'Si': 160, 'Do': 190, 'Do#': 220, 
          'Re': 250, 'Re#': 280, 'Mi': 300, 'Fa': 330, 'Fa#': 350}
CORDE5 = {'Si': 35, 'Do': 60,'Do#': 90, 'Re': 130, 'Re#': 160, 'Mi': 190, 'Fa': 220, 
          'Fa#': 250, 'Sol': 280, 'Sol#': 300, 'La': 330, 'La#': 350} 
CORDE6 = {'Mi': 35, 'Fa': 60,'Fa#': 90, 'Sol': 130, 'Sol#': 160, 'La': 190, 'La#': 220, 
          'Si': 250, 'Do': 280, 'Do#': 300, 'Re': 330, 'Re#': 350}      
          
          
         
""" Sauvergarde des gammes principales """

#NATURELLE = [0.0,1.0,1.5,2.5,3.5,4.0,5.0,6.0]
#MELODIQUE = [0.0,1.0,1.5,2.5,3.5,4.5,5.5,6.0]
#HARMONIQUE = [0.0,1.0,1.5,2.5,3.5,4.0,5.5,6.0]
#MAJEUR = [0.0,1.0,2.0,2.5,3.5,4.5,5.5,6.0]
#PENTATONIQUE = [0.0,1.5,2.5,3.5,5.0,6.0]
#BLUES = [0.0,1.5,2.5,3.0,3.5,5.0,6.0]

