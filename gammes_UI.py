# -*- coding: utf-8 -*-

import Tkinter as Tk
import generateur as Generateur
import donnees as Donnees

""" ------------------affichage des gammes sur partition-----------------------
"""
class gammes_UI(Tk.Frame):
    def __init__(self, parent):
        Tk.Frame.__init__(self, parent) # Constructeur parent
        self.parent = parent # parent eventuel
        self.canvas = Tk.Canvas(self.parent, width=550, height=100,
                             background= "white")  # creation du canvas

        self.init_canvas() # initialisation du canvas


    def init_canvas(self):
        self.canvas.create_line(0, 30, 550, 30)   # lignes de la partition
        self.canvas.create_line(0, 40, 550, 40)
        self.canvas.create_line(0, 50, 550, 50)
        self.canvas.create_line(0, 60, 550, 60)
        self.canvas.create_line(0, 70, 550, 70)
        self.cle = Tk.PhotoImage(file="img/cleSol.gif") # import gif cle de sol
        self.diese = Tk.PhotoImage(file="img/diese.gif") # import gif dieses
        self.canvas.create_image(20, 50, image=self.cle) # on colle le diese
        self.canvas.pack() # on pack le canvas

    def generer_even(self, tonalite, mode):
        self.canvas.delete(Tk.ALL) # on supprime tout le contenu du canvas
        self.init_canvas() # on le reinitialise
        self.liste_notes = Generateur.generer_gamme(tonalite, mode) # on genere la gamme en fonction de tonalite et mode
        for i, contenant in enumerate(self.liste_notes): # on enumere cette liste
            x = (i * 50 + 70) # fonction affine  pour l'abscisse de chque note
            if contenant == tonalite: # on colore en rouge les toniques
                couleur = '#ff0000'
            else: # en noir les autres notes
                couleur = '#000000'

            if (tonalite == contenant and i != 0) or Generateur.getRankDec(tonalite) > Generateur.getRankDec(contenant) :
            # affichage au dessus du do medium, soit tonique, index different de 0
            # soit le rang est inferieur à celui de la premiere tonique
                if contenant.find('La') != -1 or contenant.find('Si') != -1:
                # rajout de la barre pour le si aigu et le la aigu
                    self.canvas.create_line(x-5,20,x+20,20)

                if contenant.find('#') == -1: # Si pas de diese (=becarre)
                    self.canvas.create_oval(x, Donnees.COORD[contenant]-35,  x+15, Donnees.COORD[contenant]+10-35, fill=couleur)
                else: # si diese
                    self.canvas.create_oval(x, Donnees.COORD[contenant.replace('#', '')]-35,  # affichage
                                                 x+15, Donnees.COORD[contenant.replace('#', '')]+10-35, fill=couleur)
                    self.canvas.create_image(x-10,Donnees.COORD[contenant.replace('#', '')]+5-35, image=self.diese)

            else:        #si note medium
                if contenant.find('Do') != -1 : # barre pour le do grave
                    self.canvas.create_line(x-5,80,x+20,80)
                if contenant.find('#') == -1: # becarre
                    self.canvas.create_oval(x, Donnees.COORD[contenant],  x+15, Donnees.COORD[contenant]+10, fill=couleur)
                else: # diese
                    self.canvas.create_oval(x, Donnees.COORD[contenant.replace('#', '')],  # affichage
                                                 x+15, Donnees.COORD[contenant.replace('#', '')]+10, fill=couleur)
                    self.canvas.create_image(x-10, Donnees.COORD[contenant.replace('#', '')]+5, image=self.diese)



if __name__ == '__main__': # tests
    root = Tk()
    gammes_UI(root)
    root.mainloop()
